//
//  ViewController.m
//  p02-hull
//
//  Created by Oliver Hull on 2/2/17.
//  Copyright © 2017 Oliver Hull. All rights reserved.
//

#import "ViewController.h"
#import "CustomView.h"

@interface ViewController ()
- (int)checkLost:(int [4][4]) matrix;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (int)checkLost:(int [4][4]) matrix {
    int didLose = 1;
    int shiftLeft[4][4],shiftRight[4][4],shiftUp[4][4],shiftDown[4][4];
    
    for(int i = 0; i < 4; i++)
        for(int j = 0; j<4; j++){
            shiftLeft[i][j] = 0;
            shiftRight[i][j] = 0;
            shiftUp[i][j] = 0;
            shiftDown[i][j] = 0;
        }
    int k;
    
    //test UP
    for(int j = 0; j < 4; j++)
        for(int i = 0; i < 4; i++)
        {
            
            if(matrix[i][j] == 0) continue;
            else{
                k = i-1;
                while(1)
                {
                    if(k < 0)
                    {
                        shiftUp[k+1][j] = matrix[i][j];
                        break;
                    }
                    else if(matrix[k][j] != 0 && matrix[i][j] == matrix[k][j]){
                        shiftUp[k][j] *= 2;
                        break;
                    }
                    else if(matrix[k][j] != 0){
                        shiftUp[k+1][j] = matrix[i][j];
                        break;
                    }
                    else k--;
                }
            }
        }
    
    //test DOWN
    
    for(int j = 0; j < 4; j++)
        for(int i = 3; i >= 0; i--)
        {
            
            if(matrix[i][j] == 0) continue;
            else{
                k = i+1;
                while(1){
                    if(k > 3)
                    {
                        shiftDown[k-1][j] = matrix[i][j];
                        break;
                    }
                    else if(matrix[k][j] != 0 && matrix[i][j] == matrix[k][j]){
                        shiftDown[k][j] *= 2;
                        break;
                    }
                    else if(matrix[k][j] != 0){
                        shiftDown[k-1][j] = matrix[i][j];
                        break;
                    }
                    else k++;
                }
            }
        }
    
    
    //test RIGHT
    for(int i = 0; i < 4; i++)
        for(int j = 3; j >= 0; j--)
        {
            if(matrix[i][j] == 0) continue;
            else{
                k = j+1;
                while(1){
                    
                    if(k > 3){
                        
                        shiftRight[i][k-1] = matrix[i][j];
                        break;
                    }
                    else if(matrix[i][k] != 0 && matrix[i][j] == matrix[i][k]){
                        shiftRight[i][k]*= 2;
                        break;
                    }
                    else if(matrix[i][k]!= 0){
                        shiftRight[i][k-1] = matrix[i][j];
                        break;
                    }
                    else k++;
                }
                
            }
        }
    //test LEFT
    
    for(int i = 0; i < 4; i++)
        for(int j = 0; j < 4; j++)
        {
            if(matrix[i][j] == 0) continue;
            else{
                k = j-1;
                while(1){
                    
                    if(k < 0){
                        shiftLeft[i][k+1] = matrix[i][j];
                        break;
                    }
                    else if(matrix[i][k] != 0 && matrix[i][k] == matrix[i][j]){
                        shiftLeft[i][k] *= 2;
                        break;
                        
                    }
                    else if(matrix[i][k] != 0){
                        shiftLeft[i][k+1] = matrix[i][j];
                        break;
                    }
                    else k--;
                }
                
            }
        }
    
    
    for(int i = 0; i < 4; i++)
        for(int j = 0; j < 4; j++){
            if(matrix[i][j] != shiftUp[i][j] || matrix[i][j] != shiftDown[i][j] || matrix[i][j] != shiftLeft[i][j] || matrix[i][j] != shiftRight[i][j]) didLose = 0;
        }
    
    
    return didLose;
}

- (IBAction)reset:(id)sender{
    NSMutableArray *arr = [_cView arrayLabels];

    for(int i = 0; i<4; i++)
        for(int j = 0; j < 4; j++)
        {

            [[arr objectAtIndex: (i*4+j)] setText:@""];
            [[arr objectAtIndex: (i*4+j)] setBackgroundColor:[UIColor clearColor]];
        }
    int newPos = arc4random() % 16;
    [[arr objectAtIndex: newPos] setText:@"2"];
    [[arr objectAtIndex: newPos] setBackgroundColor:[UIColor redColor]];
    [sender removeFromSuperview];
    
    
}

- (IBAction)shiftUp:(id)sender {
    NSMutableArray *arr = [_cView arrayLabels];
    
    int matrix[4][4];
    int newMatrix[4][4];
    
    
    for(int i = 0; i<4; i++)
        for(int j = 0; j < 4; j++)
        {
            if([[[arr objectAtIndex: i*4 + j] text]  isEqual: @""]) matrix[i][j] = 0;
            else matrix[i][j] = [[arr objectAtIndex: i*4 + j] text].intValue;
            newMatrix[i][j] = 0;
        }
    
    int k;
    
    for(int j = 0; j < 4; j++)
        for(int i = 0; i < 4; i++)
        {
            
            if(matrix[i][j] == 0) continue;
            else{
                k = i-1;
                while(1)
                {
                    if(k < 0)
                    {
                        newMatrix[k+1][j] = matrix[i][j];
                        break;
                    }
                    else if(matrix[k][j] != 0 && matrix[i][j] == matrix[k][j]){
                        newMatrix[k][j] *= 2;
                        matrix[i][j] = 0;
                        break;
                    }
                    else if(matrix[k][j] != 0){
                        newMatrix[k+1][j] = matrix[i][j];
                        break;
                    }
                    else k--;
                }
            }
        }
    int didWin = 0;
    int isFull = 1;
    
    for(int i = 0; i<4; i++)
        for(int j = 0; j < 4; j++)
        {
            if(newMatrix[i][j] == 2048) didWin = 1;
            if(newMatrix[i][j] == 0){
                isFull = 0;
                [[arr objectAtIndex: (i*4+j)] setText:@""];
                [[arr objectAtIndex: (i*4+j)] setBackgroundColor:[UIColor clearColor]];
            }
            else{
                [[arr objectAtIndex: (i*4+j)] setText:[NSString stringWithFormat:@"%i", newMatrix[i][j]]];
                [[arr objectAtIndex: (i*4+j)] setBackgroundColor:[UIColor redColor]];
            }

        }
    if(didWin){
        UIButton *restartButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0, 390,400)];
        [restartButton setTitleColor:[UIColor blueColor] forState: UIControlStateNormal];
        restartButton.backgroundColor = [UIColor grayColor];
        [restartButton setTitle:@"You won! :) play again?" forState:UIControlStateNormal];
        [restartButton addTarget:self action:@selector(reset:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.view addSubview:restartButton];
    }
    else if(!isFull)
    {
        int newPos = arc4random() % 16;
        while(![[[arr objectAtIndex: newPos] text] isEqual:@""])
        {
            newPos = arc4random() % 16;
        }
        [[arr objectAtIndex: newPos] setText:@"2"];
        [[arr objectAtIndex: newPos] setBackgroundColor:[UIColor redColor]];
    }
    else if([self checkLost :matrix])
    {
        UIButton *restartButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0, 390,400)];
        [restartButton setTitleColor:[UIColor blueColor] forState: UIControlStateNormal];
        restartButton.backgroundColor = [UIColor grayColor];
        [restartButton setTitle:@"You lost :( try again?" forState:UIControlStateNormal];
        [restartButton addTarget:self action:@selector(reset:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.view addSubview:restartButton];
        
    }
    
    
}





- (IBAction)shiftDown:(id)sender {
    NSMutableArray *arr = [_cView arrayLabels];
    
    int matrix[4][4];
    int newMatrix[4][4];
    
    
    for(int i = 0; i<4; i++)
        for(int j = 0; j < 4; j++)
        {
            if([[[arr objectAtIndex: i*4 + j] text]  isEqual: @""]) matrix[i][j] = 0;
            else matrix[i][j] = [[arr objectAtIndex: i*4 + j] text].intValue;
            newMatrix[i][j] = 0;
        }
    
    int k;
    
    for(int j = 0; j < 4; j++)
        for(int i = 3; i >= 0; i--)
        {
            
            if(matrix[i][j] == 0) continue;
            else{
                k = i+1;
                while(1){
                    if(k > 3)
                    {
                        newMatrix[k-1][j] = matrix[i][j];
                        break;
                    }
                    else if(matrix[k][j] != 0 && matrix[i][j] == matrix[k][j]){
                        newMatrix[k][j] *= 2;
                        matrix[i][j] = 0;
                        break;
                    }
                    else if(matrix[k][j] != 0){
                        newMatrix[k-1][j] = matrix[i][j];
                        break;
                    }
                    else k++;
                }
            }
        }
    
    int didWin = 0;
    int isFull = 1;
    
    for(int i = 0; i<4; i++)
        for(int j = 0; j < 4; j++)
        {
            if(newMatrix[i][j] == 2048) didWin = 1;
            if(newMatrix[i][j] == 0){
                isFull = 0;
                [[arr objectAtIndex: (i*4+j)] setText:@""];
                [[arr objectAtIndex: (i*4+j)] setBackgroundColor:[UIColor clearColor]];
            }
            else{
                [[arr objectAtIndex: (i*4+j)] setText:[NSString stringWithFormat:@"%i", newMatrix[i][j]]];
                [[arr objectAtIndex: (i*4+j)] setBackgroundColor:[UIColor redColor]];
            }
            
        }
    if(didWin){
        UIButton *restartButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0, 390,400)];
        [restartButton setTitleColor:[UIColor blueColor] forState: UIControlStateNormal];
        restartButton.backgroundColor = [UIColor grayColor];
        [restartButton setTitle:@"You won! :) play again?" forState:UIControlStateNormal];
        [restartButton addTarget:self action:@selector(reset:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.view addSubview:restartButton];
    }
    else if(!isFull)
    {
        int newPos = arc4random() % 16;
        while(![[[arr objectAtIndex: newPos] text] isEqual:@""])
        {
            newPos = arc4random() % 16;
        }
        [[arr objectAtIndex: newPos] setText:@"2"];
        [[arr objectAtIndex: newPos] setBackgroundColor:[UIColor redColor]];
    }
    else if([self checkLost :matrix])
    {
        UIButton *restartButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0, 390,400)];
        [restartButton setTitleColor:[UIColor blueColor] forState: UIControlStateNormal];
        restartButton.backgroundColor = [UIColor grayColor];
        [restartButton setTitle:@"You lost :( try again?" forState:UIControlStateNormal];
        [restartButton addTarget:self action:@selector(reset:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.view addSubview:restartButton];
        
    }
    
    
}
- (IBAction)shiftLeft:(id)sender {

    NSMutableArray *arr = [_cView arrayLabels];
    
    int matrix[4][4];
    int newMatrix[4][4];
    
    
    for(int i = 0; i<4; i++)
        for(int j = 0; j < 4; j++)
        {
            if([[[arr objectAtIndex: i*4 + j] text]  isEqual: @""]) matrix[i][j] = 0;
            else matrix[i][j] = [[arr objectAtIndex: i*4 + j] text].intValue;
            newMatrix[i][j] = 0;
        }
    
    int k;
    
    for(int i = 0; i < 4; i++)
        for(int j = 0; j < 4; j++)
        {
            if(matrix[i][j] == 0) continue;
            else{
                k = j-1;
                while(1){
                    
                    if(k < 0){
                        newMatrix[i][k+1] = matrix[i][j];
                        break;
                    }
                    else if(matrix[i][k] != 0 && matrix[i][k] == matrix[i][j]){
                        newMatrix[i][k] *= 2;
                        matrix[i][j] = 0;
                        break;
                        
                    }
                    else if(matrix[i][k] != 0){
                        newMatrix[i][k+1] = matrix[i][j];
                        break;
                    }
                    else k--;
                }
                
            }
        }
    
    int didWin = 0;
    int isFull = 1;
    
    for(int i = 0; i<4; i++)
        for(int j = 0; j < 4; j++)
        {
            if(newMatrix[i][j] == 2048) didWin = 1;
            if(newMatrix[i][j] == 0){
                isFull = 0;
                [[arr objectAtIndex: (i*4+j)] setText:@""];
                [[arr objectAtIndex: (i*4+j)] setBackgroundColor:[UIColor clearColor]];
            }
            else{
                [[arr objectAtIndex: (i*4+j)] setText:[NSString stringWithFormat:@"%i", newMatrix[i][j]]];
                [[arr objectAtIndex: (i*4+j)] setBackgroundColor:[UIColor redColor]];
            }
            
        }
    if(didWin){
        UIButton *restartButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0, 390,400)];
        [restartButton setTitleColor:[UIColor blueColor] forState: UIControlStateNormal];
        restartButton.backgroundColor = [UIColor grayColor];
        [restartButton setTitle:@"You won! :) play again?" forState:UIControlStateNormal];
        [restartButton addTarget:self action:@selector(reset:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.view addSubview:restartButton];
    }
    else if(!isFull)
    {
        int newPos = arc4random() % 16;
        while(![[[arr objectAtIndex: newPos] text] isEqual:@""])
        {
            newPos = arc4random() % 16;
        }
        [[arr objectAtIndex: newPos] setText:@"2"];
        [[arr objectAtIndex: newPos] setBackgroundColor:[UIColor redColor]];
    }
    else if([self checkLost :matrix])
    {
        UIButton *restartButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0, 390,400)];
        [restartButton setTitleColor:[UIColor blueColor] forState: UIControlStateNormal];
        restartButton.backgroundColor = [UIColor grayColor];
        [restartButton setTitle:@"You lost :( try again?" forState:UIControlStateNormal];
        [restartButton addTarget:self action:@selector(reset:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.view addSubview:restartButton];
        
    }
    
}


- (IBAction)shiftRight:(id)sender {
    NSMutableArray *arr = [_cView arrayLabels];
    
    int matrix[4][4];
    int newMatrix[4][4];
    
    
    for(int i = 0; i<4; i++)
        for(int j = 0; j < 4; j++)
        {
            if([[[arr objectAtIndex: i*4 + j] text]  isEqual: @""]) matrix[i][j] = 0;
            else matrix[i][j] = [[arr objectAtIndex: i*4 + j] text].intValue;
            newMatrix[i][j] = 0;
        }
    
    int k;
 
    for(int i = 0; i < 4; i++)
        for(int j = 3; j >= 0; j--)
        {
            if(matrix[i][j] == 0) continue;
            else{
                k = j+1;
                while(1){
                    
                    if(k > 3){
                        
                        newMatrix[i][k-1] = matrix[i][j];
                        break;
                    }
                    else if(matrix[i][k] != 0 && matrix[i][j] == matrix[i][k]){
                        newMatrix[i][k]*= 2;
                        matrix[i][j] = 0;
                        break;
                    }
                    else if(matrix[i][k]!= 0){
                        newMatrix[i][k-1] = matrix[i][j];
                        break;
                    }
                    else k++;
                }
                
            }
        }
    
    int didWin = 0;
    int isFull = 1;
    
    for(int i = 0; i<4; i++)
        for(int j = 0; j < 4; j++)
        {
            if(newMatrix[i][j] == 2048) didWin = 1;
            if(newMatrix[i][j] == 0){
                isFull = 0;
                [[arr objectAtIndex: (i*4+j)] setText:@""];
                [[arr objectAtIndex: (i*4+j)] setBackgroundColor:[UIColor clearColor]];
            }
            else{
                [[arr objectAtIndex: (i*4+j)] setText:[NSString stringWithFormat:@"%i", newMatrix[i][j]]];
                [[arr objectAtIndex: (i*4+j)] setBackgroundColor:[UIColor redColor]];
            }
            
        }
    if(didWin){
        UIButton *restartButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0, 390,400)];
        [restartButton setTitleColor:[UIColor blueColor] forState: UIControlStateNormal];
        restartButton.backgroundColor = [UIColor grayColor];
        [restartButton setTitle:@"You won! :) play again?" forState:UIControlStateNormal];
        [restartButton addTarget:self action:@selector(reset:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.view addSubview:restartButton];
    }
    else if(!isFull)
    {
        int newPos = arc4random() % 16;
        while(![[[arr objectAtIndex: newPos] text] isEqual:@""])
        {
            newPos = arc4random() % 16;
        }
        [[arr objectAtIndex: newPos] setText:@"2"];
        [[arr objectAtIndex: newPos] setBackgroundColor:[UIColor redColor]];
    }
    else if([self checkLost :matrix])
    {
        UIButton *restartButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0, 390,400)];
        [restartButton setTitleColor:[UIColor blueColor] forState: UIControlStateNormal];
        restartButton.backgroundColor = [UIColor grayColor];
        [restartButton setTitle:@"You lost :( try again?" forState:UIControlStateNormal];
        [restartButton addTarget:self action:@selector(reset:) forControlEvents:(UIControlEventTouchUpInside)];
        [self.view addSubview:restartButton];
        
    }
    
}
@end
