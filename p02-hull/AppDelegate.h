//
//  AppDelegate.h
//  p02-hull
//
//  Created by Oliver Hull on 2/2/17.
//  Copyright © 2017 Oliver Hull. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

