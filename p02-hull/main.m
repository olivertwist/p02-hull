//
//  main.m
//  p02-hull
//
//  Created by Oliver Hull on 2/2/17.
//  Copyright © 2017 Oliver Hull. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
