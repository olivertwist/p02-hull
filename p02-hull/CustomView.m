//
//  CustomView.m
//  p02-hull
//
//  Created by Oliver Hull on 2/2/17.
//  Copyright © 2017 Oliver Hull. All rights reserved.
//

#import "CustomView.h"
#import "stdlib.h"
@implementation CustomView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }

*/

- (void)drawRect:(CGRect)rect {
   


    
    UIBezierPath *path = [UIBezierPath bezierPath];
    int start,end,coord;
    start = 0;
    end = 500;

    for(int i = 0; i <= 4; i++)
    {
        coord = (i*96)+3;
        [path moveToPoint:CGPointMake(coord, start)];

        [path addLineToPoint:CGPointMake(coord,end)];
    
        path.lineWidth = 10;

        [[UIColor blueColor] setStroke];
        [path stroke];
       
        coord = (i*96)+3;
        [path moveToPoint:CGPointMake(start,coord)];
        
        [path addLineToPoint:CGPointMake(end,coord)];
        
        [path stroke];
    }

    _arrayLabels = [NSMutableArray array];
    

     for(int i = 0; i < 4; i++)
        for(int j = 0; j<4; j++)
        {
            UILabel *cell = [[UILabel alloc] initWithFrame:CGRectMake(8 +j*96, 8+ i*96, 86, 86)];
            [cell setTextColor:[UIColor blackColor]];
            [cell setBackgroundColor:[UIColor clearColor]];
            [cell setText:@""];
            [cell setTextAlignment:NSTextAlignmentCenter];
            [self addSubview:cell];
            [_arrayLabels addObject:cell];

        }

    UIButton *startButton = [[UIButton alloc] initWithFrame:CGRectMake(0,0, 390,390)];
    [startButton setTitleColor:[UIColor blueColor] forState: UIControlStateNormal];
    startButton.backgroundColor = [UIColor grayColor];
    [startButton setTitle:@"START" forState:UIControlStateNormal];
    [startButton addTarget:self action:@selector(initializeGame:) forControlEvents:(UIControlEventTouchUpInside)];
    [self addSubview:startButton];
    
    
    

}
- (IBAction)initializeGame:(id)sender
{
    int start = (arc4random() % 16);
    [[_arrayLabels objectAtIndex: start] setText:@"2"];
    [[_arrayLabels objectAtIndex: start] setBackgroundColor:[UIColor redColor]];
    [sender removeFromSuperview];
    
}

- (NSMutableArray *)arrayLabels
{
    return [NSMutableArray arrayWithArray:_arrayLabels];
}
@end





























