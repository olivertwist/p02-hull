//
//  ViewController.h
//  p02-hull
//
//  Created by Oliver Hull on 2/2/17.
//  Copyright © 2017 Oliver Hull. All rights reserved.
//
#import "CustomView.h"
#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property NSMutableArray * labels;
@property (weak, nonatomic) IBOutlet CustomView *cView;

@end

